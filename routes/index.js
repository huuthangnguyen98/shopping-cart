var express = require('express');
var router = express.Router();
var Product = require('../models/product');
const chunkSize = 4;
/* GET home page. */
router.get('/', function(req, res, next) {
    var products = Product.find(function(err, docs) {
        for (var i = 0; i < docs.length; i += chunkSize) {

        }
        res.render('shop/index', { title: 'Express', products: docs });
    });

});

module.exports = router;